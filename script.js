function solveExpression(expression, isDegreeMode) {
  let _convertedExpression = expression.replace(/resin\(/g, "( 392.9 * ");
  if (isDegreeMode) {
    _convertedExpression = _convertedExpression
      .replace(/sin\(/g, "sin( (pi / 180) * ")
      .replace(/cos\(/g, "cos( (pi / 180) * ");
  }
  return math.evaluate(_convertedExpression);
}

function performCalculations() {
  const expression = document.getElementById("inputBox").value;
  const degreeMode = document.getElementById("degree-checkbox").checked;
  try {
    const computedResult = solveExpression(expression, degreeMode);
    if (typeof computedResult === 'number') {
      const answerElement = document.getElementById("answerParagraph");
      answerElement.innerText = computedResult;
      answerElement.classList.remove("error");
    } else {
      throw new Error("Invalid expression !!!");
    }
  } catch (e) {
    const answerElement = document.getElementById("answerParagraph");
    answerElement.classList.add("error");
    answerElement.innerText = "Invalid expression!!!";
  }
}
